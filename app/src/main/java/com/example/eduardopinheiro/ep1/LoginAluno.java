package com.example.eduardopinheiro.ep1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class LoginAluno extends AppCompatActivity {
    EditText txtNUSP;
    EditText txtSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_aluno);
        txtNUSP = (EditText) findViewById(R.id.txtNUSPAlunoLogin);
        txtSenha = (EditText) findViewById(R.id.txtPassAlunoLogin);
    }

    public void loginAluno(View v)
    {
        Intent intent = new Intent(getApplicationContext(), AreaAluno.class);
        startActivity(intent);
    }

    public void limparTela(View v)
    {
        txtNUSP.setText("");
        txtSenha.setText("");
    }

    public void cadastrarAluno(View v)
    {
        Intent intent = new Intent(getApplicationContext(), CadastroAluno.class);
        startActivity(intent);
    }
}
