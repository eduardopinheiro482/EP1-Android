package com.example.eduardopinheiro.ep1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SelecionarSeminario extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selecionar_seminario);
    }

    public void detalhesSeminario(View v)
    {
        Intent intent = new Intent(getApplicationContext(), SeminarioDetalhe.class);
        startActivity(intent);
    }
}
