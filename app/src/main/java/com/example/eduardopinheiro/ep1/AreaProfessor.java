package com.example.eduardopinheiro.ep1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class AreaProfessor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_professor);
    }

    public void alterarCadastro(View v)
    {
        Intent intent = new Intent(getApplicationContext(), AlterarProfessor.class);
        startActivity(intent);
    }

    public void addSeminario(View v)
    {
        Intent intent = new Intent(getApplicationContext(), CadastroSeminario.class);
        startActivity(intent);
    }

    public void detalhesSeminario(View v)
    {
        Intent intent = new Intent(getApplicationContext(), SelecionarSeminario.class);
        startActivity(intent);
    }
}
