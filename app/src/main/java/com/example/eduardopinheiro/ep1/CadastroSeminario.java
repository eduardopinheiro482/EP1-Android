package com.example.eduardopinheiro.ep1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class CadastroSeminario extends AppCompatActivity {
    EditText txtNome;
    TextView labelLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_seminario);
        txtNome = (EditText) findViewById(R.id.txtNomeCSeminario);
        labelLog = (TextView) findViewById(R.id.labelLogCSeminario);
    }

    public void cadastrarSeminario(View v)
    {
        labelLog.setText("SUCCESS");
    }

    public void limparTela(View v)
    {
        txtNome.setText("");
        labelLog.setText("");
    }
}
