package com.example.eduardopinheiro.ep1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class AlterarAluno extends AppCompatActivity {
    EditText txtNome;
    EditText txtNUSP;
    EditText txtSenha;
    TextView labelLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_aluno);
        txtNome = (EditText) findViewById(R.id.txtNomeAAluno);
        txtNUSP = (EditText) findViewById(R.id.txtNUSPAAluno);
        txtSenha = (EditText) findViewById(R.id.txtPassAAluno);
        labelLog = (TextView) findViewById(R.id.labelLogAAluno);
    }

    public void alterarAluno(View v)
    {
        labelLog.setText("SUCCESS");
    }

    public void limparTela(View v)
    {
        txtNome.setText("");
        txtNUSP.setText("");
        txtSenha.setText("");
        labelLog.setText("");
    }
}
