package com.example.eduardopinheiro.ep1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class CadastroAluno extends AppCompatActivity {
    EditText txtNome;
    EditText txtNUSP;
    EditText txtSenha;
    TextView labelLog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_aluno);
        txtNome = (EditText) findViewById(R.id.txtNomeCAluno);
        txtNUSP = (EditText) findViewById(R.id.txtNUSPCAluno);
        txtSenha = (EditText) findViewById(R.id.txtPassCAluno);
        labelLog = (TextView) findViewById(R.id.labelLogCALuno);

    }

    public void cadastrarAluno(View v)
    {
        labelLog.setText("SUCCESS");
    }

    public void limparTela(View v)
    {
        txtNome.setText("");
        txtNUSP.setText("");
        txtSenha.setText("");
        labelLog.setText("");
    }
}
