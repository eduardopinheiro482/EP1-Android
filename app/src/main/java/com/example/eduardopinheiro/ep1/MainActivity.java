package com.example.eduardopinheiro.ep1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void alunoLogin(View v)
    {
        Intent intent = new Intent(getApplicationContext(), LoginAluno.class);
        startActivity(intent);
    }

    public void professorLogin(View v)
    {
        Intent intent = new Intent(getApplicationContext(), LoginProfessor.class);
        startActivity(intent);
    }
}
