package com.example.eduardopinheiro.ep1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class AlterarProfessor extends AppCompatActivity {
    EditText txtNome;
    EditText txtNUSP;
    EditText txtSenha;
    TextView labelLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alterar_professor);
        txtNome = (EditText) findViewById(R.id.txtNomeAProfessor);
        txtNUSP = (EditText) findViewById(R.id.txtNUSPAProfessor);
        txtSenha = (EditText) findViewById(R.id.txtPassAProfessor);
        labelLog = (TextView) findViewById(R.id.labelLogAProfessor);
    }

    public void alterarProfessor(View v)
    {
        labelLog.setText("SUCCESS");
    }

    public void limparTela(View v)
    {
        txtNome.setText("");
        txtNUSP.setText("");
        txtSenha.setText("");
        labelLog.setText("");
    }
}
