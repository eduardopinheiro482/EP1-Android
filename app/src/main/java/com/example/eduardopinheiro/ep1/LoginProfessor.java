package com.example.eduardopinheiro.ep1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class LoginProfessor extends AppCompatActivity {
    EditText txtNUSP;
    EditText txtSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_professor);
        txtNUSP = (EditText) findViewById(R.id.txtNUSPProfessorLogin);
        txtSenha = (EditText) findViewById(R.id.txtPassProfessorLogin);
    }

    public void professorLogin(View v)
    {
        Intent intent = new Intent(getApplicationContext(), AreaProfessor.class);
        startActivity(intent);
    }

    public void limparTela(View v)
    {
        txtNUSP.setText("");
        txtSenha.setText("");
    }

    public  void cadastrarProfessor(View v)
    {
        Intent intent = new Intent(getApplicationContext(), CadastroProfessor.class);
        startActivity(intent);
    }
}
